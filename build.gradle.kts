buildscript {
  repositories {
    jcenter()
  }
}

allprojects {
  repositories {
    maven("https://dl.bintray.com/kotlin/kotlin-dev/")
  }
}
